<nav class="navbar navbar-expand-lg nav-rojo shadow-sm navbar-light" style="padding-right: 160px">
    <div class="container-fluid">
        <p>
            <a class="text-white navbar-brand" href="{{ route('home') }}">
                {{-- {{ config('app.name') }} --}}
                <img style="max-width: 45%" src="/img/logo_althacare.png" alt="" srcset="">
            </a>
        </p>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <form action="../../form-result.php" method="post" target="_blank" style="width: 600px">       
            <div class="row">
                <div class="col-6">
                    <input type="search" name="busquedamodelos" list="listamodelos" placeholder="Busca entre nuestros modelos:" class="form-control ds-input">
                </div>
                <div class="col-6">
                    <input type="submit" value="Buscar" class="btn btn-secondary">
                </div>
            </div>
        </form>
          
        <datalist id="listamodelos">
        
            <option value="Camaro">
            
            <option value="Corvette">
            
            <option value="Impala">
            
            <option value="Colorado">
        
        </datalist>
        
        <div class="collapse navbar-collapse mr-4" id="navbarSupportedContent">
            <ul class="nav mr-4 align-items-center d-flex">
                @auth
                <li class="nav-item">
                    <button type="button" class="text-white btn btn-outline-secondary">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-check" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"></path>
                            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"></path>
                            <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"></path>
                        </svg>
                        <span class="visually-hidden">Button</span>
                      </button>
                </li>
                @endauth
                
                
                
                
                    <li class="nav-item dropdown mx-5">
                        <a class="text-white nav-link btn btn-secondary btn-outline-secondary" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            <span class="navbar-toggler-icon"></span>
                        </a>
                        <ul class="dropdown-menu mx-1 px-1 dropdown-menu-lg-end">
                            @guest
                            <li class="mr-4 dropdown-item"><a class="nav-link {{ setActive('home') }}" href="{{ route('login') }}">Iniciar sesión</a></li>
                            @else
                            <li class="mr-4 text-center my-2"><img src="/img/roche-logo.svg.png" style="width: 40%" alt="" srcset=""></li>
                            <li class="mr-4 text-center my-2 px-3 text-primary">{{ request()->user()->email }}</li>
                            <li class="mr-4 dropdown-item text-center"><a class="nav-link {{ setActive('contacto') }}" href="#" onclick="">Administrar cuenta</a></li>
                            <li class="mr-4 dropdown-item text-center"><a class="nav-link {{ setActive('contacto') }}" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar sesión</a></li>
                            @endguest
                            {{-- <li><hr class="dropdown-divider"></li> --}}
                        </ul>
                    </li>
                
                
            </ul>
        </div>
    </div>
    
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>