@extends('layout')
@section('titulo','Reportes')
@section('content')
<div class="mt-5">
    <h1 class="mb-0 display-4 mb-4">Reportes</h1>    
    <div class="row">
        <div class="col-6 p-4">{{$chart->container() }}</div>
        <div class="col-6 p-4">{{$chart2->container() }}</div>
        <div class="col-6 p-4">{{$chart3->container() }}</div>
        <div class="col-6 p-4">{{$chart4->container() }}</div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset=utf-8></script>
    {{ $chart->script() }}
    {{ $chart2->script() }}
    {{ $chart3->script() }}
    {{ $chart4->script() }}
    <script>

        // JQuery
        // $(document).ready(function () {
        //     $('#submit_expediente').click(function (e) { 
        //         let fotografias=document.getElementById('fotografias').files.length;
        //         if (fotografias!=6) {
        //             e.preventDefault();
        //             alert("Deben ser 6 fotografias de la caja");   
        //         }                
        //     });
        // });

        // JS
        // let submit_btn=document.getElementById('submit_expediente');
        // submit_btn.addEventListener('click',function () {
        //     let fotografias = document.getElementById('fotografias');
        //     if (fotografias.files.length<6) {
        //         alert("Tienes menos de 6");
        //     } else {
        //         if (fotografias.files.length==6) {
        //             alert("OK");
        //             document.getElementById('expediente').validate();
        //         } else {
        //             alert("Tienes mas de 6");
        //         }
        //     }            
        // });

    </script>
</div>
@endsection
