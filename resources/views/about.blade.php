@extends('layout')
@section('titulo','About')
@section('content')
<div class="container">
    <div class="row">
          <div class="col-12 col-lg-6">
              <img class="img-fluid" src="/img/about.svg" alt="">
          </div>
          <div class="col-12 col-lg-6">
            <h1 class="display-4 text-primary">Quien soy</h1>
            <p class="lead text-secondary">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum auctor diam quis euismod. Morbi eu ante eget felis venenatis blandit at in lectus. Suspendisse maximus eget est lacinia suscipit. Maecenas in est in libero suscipit lacinia quis vitae mauris. Donec sit amet erat sed lacus faucibus viverra. Vivamus pellentesque, magna vel porta fermentum, sem eros feugiat nibh, in posuere est dui ut dolor. Vestibulum eu ex sit amet arcu gravida lacinia. Quisque congue cursus lacus non molestie. Curabitur consectetur nibh non quam lobortis, sit amet porta odio iaculis. Nullam sed fringilla orci. Fusce dignissim justo non felis eleifend, sed facilisis erat lacinia. Aliquam lobortis nisi in odio tempus dapibus. Curabitur feugiat nunc risus, et laoreet lectus dictum tincidunt. Quisque metus ex, pretium sit amet est vel, vestibulum vulputate mauris.
            </p>
            {{-- <div class="d-grid gap-2">
                <a href="{{ route('contacto') }}" class="text-white btn btn-lg btn-block btn-primary">
                    Contáctame
                </a>
                <a href="{{ route('project.index') }}" class="btn btn-lg btn-block btn-outline-primary">
                    Contáctame
                </a>
            </div> --}}
        </div>
    </div>
  </div>
@endsection
