<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo','La capital')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> --}}
    {{-- <script src="({{ mix('js/app.js') }})" defer></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <div id="app" class="d-flex flex-column h-screen justify-content-between">
        <header>
            @include('partials.nav')
            <div style="border: none; background-color: #e00000" class="container-fluid text-white py-2">Consola Roche</div>
            @include('partials.session-status')
        </header>
        <div>
            <main class="">
                <div class="container-fluid">
                    
                    <div class="row justify-content-center">
                        @auth
                        <div class="col-2 ">
                            <ul class="sticky-top mb-5 mt-5 pt-5 nav nav-pills justify-content-center">
                                <li class="container btn pt-4  px-5 mt-4">
                                    <a href="{{ route('home') }}" class="py-4 nav-link h4 nav-item p-4 {{ setActive('home') }}">Inicio</a>
                                </li>
                                <li class="container px-5  btn">
                                    <a href="{{ route('project.index') }}" class="py-4 nav-link h4 nav-item p-4 {{ setActive('project.*') }}">Panel</a>
                                </li>
                                <li class="container px-5  btn">
                                    <a href="" class="py-4 nav-link h4 nav-item p-4 {{ setActive('') }}">Facturación</a>
                                </li>
                                <li class="container px-5  btn">
                                    <a href="" class="py-4 nav-link h4 nav-item p-4 {{ setActive('') }}">Jurídico</a>
                                </li>
                                <li class="container px-5  btn">
                                    <a href="" class="py-4 nav-link h4 nav-item p-4 {{ setActive('') }}">Configuración</a>
                                </li>
                                <li class="container px-5  btn">
                                    <a href="{{ route('reportes') }}" class="py-4 nav-link h4 nav-item p-4 {{ setActive('reportes') }}">Reportes</a>
                                </li>
                            </ul>
                        </div>
                        @endauth
                        
                        <div class="col-10">
                            @yield('content')
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
                    <a href="https://api.whatsapp.com/send?phone=51955081075&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20Varela%202." class="float" target="_blank">
                        <i class="fa fa-whatsapp my-float"></i>
                    </a>
                        </div>
                    </div>
                    
                </div>
                @if (request()->routeIs("home"))
                <div class="div-gris-claro container-fluid py-5 text-center px-5">
                    <h3 class="px-5 mx-5">
                        <div class="px-5 txt-red fw-normal">
                            <a href="{{ route('project.create') }}" class="text-white btn btn-lg btn-block btn-primary">
                                Agregar paciente nuevo 
                            </a>
                        </div>
                    </h3>
                </div>
                @endif
                
            </main>
        </div>        
        {{-- <footer class="bg-white text-black-50 text-center py-3 shadow">
            {{ config('app.name') }} | Copyright @ {{ date('Y') }}
        </footer> --}}
        @include('partials.footer')
        
        
    </div>
    
</body>
</html>