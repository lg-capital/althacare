@extends('layout')
@section('titulo','Contacto')
@section('content')
<div class="container my-3">
    <div class="row">
        <div class="col-12 col-sm-10 col-md-10 col-xl-6 mx-auto">
            <form class="bg-white shadow rounded py-3 px-3" action="{{ route('contacto') }}" method="POST">
                @csrf
                <h1 class="display-4">Contacto</h1>
                <hr>
                <div class="mb-3">            
                    <label class="form-label" for="">Nombre</label>
                    <input class="@error('nombre') is-invalid @else border-0 @enderror form-control bg-ligth shadow-sm"  type="text" name="nombre" placeholder="Nombre..." value="{{ old('nombre') }}">
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div> 
                <div class="mb-3">
                    <label class="form-label" for="">Email</label>
                    <input class="@error('email') is-invalid @else border-0 @enderror form-control bg-ligth shadow-sm" type="email" name="email" placeholder="Email..." value="{{ old('email') }}">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>        
                <div class="mb-3">
                    <label class="form-label" for="">Mensaje</label>
                    <textarea class="@error('mensaje') is-invalid @else border-0 @enderror form-control bg-ligth shadow-sm" name="mensaje" id="" cols="30" rows="10" placeholder="Mensaje...">{{ old('mensaje') }}</textarea>
                    @error('mensaje')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>            
                <div class="d-grid gap-2">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
                </div>
            </form>
        </div>
    </div>
    
    
</div>

@endsection
