@extends('layout')
@section('titulo','Expendiente')
@section('content')
<div class="">    
    <form class="p-3 mt-2 was-validated" id="expediente" name="expediente">
        @csrf
        <div class="justify-content-md-center row">
            <div class="col-12 col-lg-6 p-3 bg-white shadow rounded">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                      <button class="nav-link active" id="nav-paciente-tab" data-bs-toggle="tab" data-bs-target="#nav-paciente" type="button" role="tab" aria-controls="nav-paciente" aria-selected="true">Paciente</button>
                      <button class="nav-link  " id="nav-medico-tab" data-bs-toggle="tab" data-bs-target="#nav-medico" type="button" role="tab" aria-controls="nav-medico" aria-selected="false">Medico</button>
                      <button class="nav-link  " id="nav-producto-tab" data-bs-toggle="tab" data-bs-target="#nav-producto" type="button" role="tab" aria-controls="nav-producto" aria-selected="false">Producto</button>
                      <button class="nav-link  " id="nav-expediente-tab" data-bs-toggle="tab" data-bs-target="#nav-expediente" type="button" role="tab" aria-controls="nav-expediente" aria-selected="false">Expediente</button>
                      <button class="nav-link  " id="nav-import-export-tab" data-bs-toggle="tab" data-bs-target="#nav-import-export" type="button" role="tab" aria-controls="nav-import-export" aria-selected="false">Import/Export</button>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">

                    {{-- Paciente --}}

                    <div class="tab-pane fade show active" id="nav-paciente" role="tabpanel" aria-labelledby="nav-paciente-tab">             
                        <div class="">
                            <h1 class="mb-4">Información del paciente</h1>
                            <div class="form-group row">
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="nombres_paciente">Nombres</label>
                                    <div>
                                        <input class="form-control" id="nombres_paciente" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="appellido_paterno_paciente">Apellido paterno</label>
                                    <div>
                                        <input class="form-control" id="appellido_paterno_paciente" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="apellido_materno_paciente">Apellido materno</label>
                                    <div>
                                        <input class="form-control" id="appellido_materno_paciente" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="nacimiento" class="input-group-text">Fecha de nacimiento</label>
                                    <div class="">
                                        <div>
                                            <input type="date" class="form-control" id="nacimiento" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="edad">Edad</label>
                                    <div>
                                        <div>
                                            <input type="number" class="form-control" id="edad" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mb-3 col-md-6">
                                    <div class="card">
                                        <label class="card-header">Sexo</label>
                                        <div class="card-body justify-content-evenly d-flex">
                                            <div class="form-check-inline">
                                                <input class="" id="masculino" type="radio" name="sexo" checked required>
                                                <label class="form-check-label" for="masculino">
                                                    Masculino
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input class="" id="femenino" type="radio" name="sexo" required>
                                                <label class="form-check-label" for="femenino">
                                                    Femenino
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="calle_o_avenida">Calle o avenida</label>
                                    <div>
                                        <input type="text" id="calle_o_avenida" class="form-control" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="numero_interior">Numero interior</label>
                                    <div>
                                        <input type="number" class="form-control" id="numero_interior" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="numero_exterior">Numero exterior</label>
                                    <div>
                                        <input type="number" class="form-control" id="numero_exterior" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="colonia_o_fraccionamiento">Colonia o fraccionamiento</label>
                                    <div>
                                        <input type="text" id="colonia_o_fraccionamiento" class="form-control" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="codigo_postal">Codigo postal</label>
                                    <div>
                                        <input type="number" class="form-control" id="codigo_postal" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="ciudad_paciente">Ciudad</label>
                                    <div>
                                        <input class="form-control" id="ciudad_paciente" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="estado_paciente">Estado</label>
                                    <div>
                                        <input class="form-control" id="estado_paciente" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="pais">País</label>
                                    <div>
                                        <select class="form-select" id="pais_paciente" aria-label="Default select example" required>
                                            <option value="" selected disabled>Seleccione una opción</option>
                                            <option value="AF">Afganistán</option>
                                            <option value="AL">Albania</option>
                                            <option value="DE">Alemania</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antártida</option>
                                            <option value="AG">Antigua y Barbuda</option>
                                            <option value="AN">Antillas Holandesas</option>
                                            <option value="SA">Arabia Saudí</option>
                                            <option value="DZ">Argelia</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaiyán</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrein</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BE">Bélgica</option>
                                            <option value="BZ">Belice</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermudas</option>
                                            <option value="BY">Bielorrusia</option>
                                            <option value="MM">Birmania</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia y Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BR">Brasil</option>
                                            <option value="BN">Brunei</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="BT">Bután</option>
                                            <option value="CV">Cabo Verde</option>
                                            <option value="KH">Camboya</option>
                                            <option value="CM">Camerún</option>
                                            <option value="CA">Canadá</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CY">Chipre</option>
                                            <option value="VA">Ciudad del Vaticano (Santa Sede)</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comores</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, República Democrática del</option>
                                            <option value="KR">Corea</option>
                                            <option value="KP">Corea del Norte</option>
                                            <option value="CI">Costa de Marfíl</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="HR">Croacia (Hrvatska)</option>
                                            <option value="CU">Cuba</option>
                                            <option value="DK">Dinamarca</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egipto</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="AE">Emiratos Árabes Unidos</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="SI">Eslovenia</option>
                                            <option value="ES">España</option>
                                            <option value="US">Estados Unidos</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Etiopía</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="PH">Filipinas</option>
                                            <option value="FI">Finlandia</option>
                                            <option value="FR">Francia</option>
                                            <option value="GA">Gabón</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GD">Granada</option>
                                            <option value="GR">Grecia</option>
                                            <option value="GL">Groenlandia</option>
                                            <option value="GP">Guadalupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GY">Guayana</option>
                                            <option value="GF">Guayana Francesa</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GQ">Guinea Ecuatorial</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="HT">Haití</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HU">Hungría</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IQ">Irak</option>
                                            <option value="IR">Irán</option>
                                            <option value="IE">Irlanda</option>
                                            <option value="BV">Isla Bouvet</option>
                                            <option value="CX">Isla de Christmas</option>
                                            <option value="IS">Islandia</option>
                                            <option value="KY">Islas Caimán</option>
                                            <option value="CK">Islas Cook</option>
                                            <option value="CC">Islas de Cocos o Keeling</option>
                                            <option value="FO">Islas Faroe</option>
                                            <option value="HM">Islas Heard y McDonald</option>
                                            <option value="FK">Islas Malvinas</option>
                                            <option value="MP">Islas Marianas del Norte</option>
                                            <option value="MH">Islas Marshall</option>
                                            <option value="UM">Islas menores de Estados Unidos</option>
                                            <option value="PW">Islas Palau</option>
                                            <option value="SB">Islas Salomón</option>
                                            <option value="SJ">Islas Svalbard y Jan Mayen</option>
                                            <option value="TK">Islas Tokelau</option>
                                            <option value="TC">Islas Turks y Caicos</option>
                                            <option value="VI">Islas Vírgenes (EEUU)</option>
                                            <option value="VG">Islas Vírgenes (Reino Unido)</option>
                                            <option value="WF">Islas Wallis y Futuna</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italia</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japón</option>
                                            <option value="JO">Jordania</option>
                                            <option value="KZ">Kazajistán</option>
                                            <option value="KE">Kenia</option>
                                            <option value="KG">Kirguizistán</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="LA">Laos</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LV">Letonia</option>
                                            <option value="LB">Líbano</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libia</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lituania</option>
                                            <option value="LU">Luxemburgo</option>
                                            <option value="MK">Macedonia, Ex-República de Yugoslava</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MY">Malasia</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MV">Maldivas</option>
                                            <option value="ML">Malí</option>
                                            <option value="MT">Malta</option>
                                            <option value="MA">Marruecos</option>
                                            <option value="MQ">Martinica</option>
                                            <option value="MU">Mauricio</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">México</option>
                                            <option value="FM">Micronesia</option>
                                            <option value="MD">Moldavia</option>
                                            <option value="MC">Mónaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Níger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk</option>
                                            <option value="NO">Noruega</option>
                                            <option value="NC">Nueva Caledonia</option>
                                            <option value="NZ">Nueva Zelanda</option>
                                            <option value="OM">Omán</option>
                                            <option value="NL">Países Bajos</option>
                                            <option value="PA">Panamá</option>
                                            <option value="PG">Papúa Nueva Guinea</option>
                                            <option value="PK">Paquistán</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Perú</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PF">Polinesia Francesa</option>
                                            <option value="PL">Polonia</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="UK">Reino Unido</option>
                                            <option value="CF">República Centroafricana</option>
                                            <option value="CZ">República Checa</option>
                                            <option value="ZA">República de Sudáfrica</option>
                                            <option value="DO">República Dominicana</option>
                                            <option value="SK">República Eslovaca</option>
                                            <option value="RE">Reunión</option>
                                            <option value="RW">Ruanda</option>
                                            <option value="RO">Rumania</option>
                                            <option value="RU">Rusia</option>
                                            <option value="EH">Sahara Occidental</option>
                                            <option value="KN">Saint Kitts y Nevis</option>
                                            <option value="WS">Samoa</option>
                                            <option value="AS">Samoa Americana</option>
                                            <option value="SM">San Marino</option>
                                            <option value="VC">San Vicente y Granadinas</option>
                                            <option value="SH">Santa Helena</option>
                                            <option value="LC">Santa Lucía</option>
                                            <option value="ST">Santo Tomé y Príncipe</option>
                                            <option value="SN">Senegal</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leona</option>
                                            <option value="SG">Singapur</option>
                                            <option value="SY">Siria</option>
                                            <option value="SO">Somalia</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="PM">St Pierre y Miquelon</option>
                                            <option value="SZ">Suazilandia</option>
                                            <option value="SD">Sudán</option>
                                            <option value="SE">Suecia</option>
                                            <option value="CH">Suiza</option>
                                            <option value="SR">Surinam</option>
                                            <option value="TH">Tailandia</option>
                                            <option value="TW">Taiwán</option>
                                            <option value="TZ">Tanzania</option>
                                            <option value="TJ">Tayikistán</option>
                                            <option value="TF">Territorios franceses del Sur</option>
                                            <option value="TP">Timor Oriental</option>
                                            <option value="TG">Togo</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad y Tobago</option>
                                            <option value="TN">Túnez</option>
                                            <option value="TM">Turkmenistán</option>
                                            <option value="TR">Turquía</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UA">Ucrania</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistán</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Vietnam</option>
                                            <option value="YE">Yemen</option>
                                            <option value="YU">Yugoslavia</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabue</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="telefono_paciente">Telefono de contacto</label>
                                    <div>
                                        <input class="form-control" id="telefono_paciente" required>
                                    </div>
                                </div>
                                <div class="mb-3 col-6">
                                    <label class="input-group-text" for="email_paciente">Correo de contacto</label>
                                    <div>
                                        <input class="form-control" id="email_paciente" required>
                                    </div>
                                </div>
                            </div>
                        </div>        
                        <div class="d-flex justify-content-end">                 
                            <a type="button" onclick="document.getElementById('siguiente_1').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-right-square-fill" viewBox="0 0 16 16">
                                    <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z"/>
                                </svg>
                            </a>
                            <button id="siguiente_1" data-bs-toggle="tab" data-bs-target="#nav-paciente" class="d-none"></button>
                        </div>
                    </div>

                    {{-- Medico --}}

                    <div class="tab-pane fade" id="nav-medico" role="tabpanel" aria-labelledby="nav-medico-tab">
                        <div class="">
                            <h1 class="mb-4">Medico tratante</h1>
                            <div class="form-group row">
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="nombres_medico">Nombres</label>
                                    <div>
                                        <input class="form-control" id="nombres_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="apellido_paterno_medico">Apellido paterno</label>
                                    <div>
                                        <input class="form-control" id="apellido_paterno_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="apellido_materno_medico">Apellido materno</label>
                                    <div>
                                        <input class="form-control" id="apellido_materno_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="ciudad_medico">Ciudad</label>
                                    <div>
                                        <input class="form-control" id="ciudad_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="estado_medico">Estado</label>
                                    <div>
                                        <input class="form-control" id="estado_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="telefono_medico">Telefono de contacto</label>
                                    <div>
                                        <input class="form-control" id="telefono_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="email_medico">Correo de contacto</label>
                                    <div>
                                        <input class="form-control" id="email_medico" required>
                                    </div>
                                </div>
                                <div class=" col-6 mb-3">
                                    <label class="input-group-text" for="cedula">Cedula</label>
                                    <div>
                                        <input class="form-control" id="cedula" required>
                                    </div>
                                </div>
                                <hr class="mt-4">
                                <h3 class="mb-4">Especialidad</h3>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="especialidad">Especialidad</label>
                                    <div>
                                        <input class="form-control" id="especialidad" required>
                                    </div>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="subespecialidad">Subespecialidad</label>
                                    <div>
                                        <input class="form-control" id="subespecialidad" required>
                                    </div>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="cedula_especialidad">Cedula de especialidad</label>
                                    <div>
                                        <input class="form-control" id="cedula_especialidad" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <button id="siguiente_0" data-bs-toggle="tab" data-bs-target="#nav-paciente" class="d-none"></button>
                            <a type="button" onclick="document.getElementById('siguiente_0').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                    <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                </svg>
                            </a>
                            <a type="button" onclick="document.getElementById('siguiente_2').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-right-square-fill" viewBox="0 0 16 16">
                                    <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z"/>
                                </svg>
                            </a>
                            <button id="siguiente_2" class="d-none" data-bs-toggle="tab" data-bs-target="#nav-medico"></button>
                        </div>
                    </div>

                    {{-- Producto --}}

                    <div class="tab-pane fade" id="nav-producto" role="tabpanel" aria-labelledby="nav-producto-tab">
                        <div>
                            <h1 class="mb-4">Producto</h1>
                            <div class="form-group row">
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="marca">Marca</label>
                                    <input class="form-control" id="marca" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="principio_activo">Principio activo</label>
                                    <input class="form-control" id="principio_activo" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="presentacion">Presentación</label>
                                    <input class="form-control" id="presentacion" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="lote">Lote</label>
                                    <input class="form-control" id="lote" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label for="caducidad" class="input-group-text">Fecha de caducidad</label>
                                    <div class="">
                                        <input class="form-control" type="date" id="caducidad" required>
                                      </div>
                                  </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <div class="card">
                                        <label class="card-header">Almacenamiento</label>
                                        <div class="card-body justify-content-evenly d-flex">
                                            <div class="form-check-inline">
                                                <input class="" id="seco" type="radio" name="almacenamiento" checked required>
                                                <label class="form-check-label" for="seco">
                                                    Seco
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input class="" id="refrigerado" type="radio" name="almacenamiento" required>
                                                <label class="form-check-label" for="refrigerado">
                                                    Refrigerado
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fabricante">Fabricante</label>
                                    <input class="form-control" id="fabricante" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="pais">País de origen</label>
                                    <select class="form-select" id="pais_producto" aria-label="Default select example" required>
                                        <option value="" selected disabled>Seleccione una opción</option>
                                        <option value="AF">Afganistán</option>
                                        <option value="AL">Albania</option>
                                        <option value="DE">Alemania</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AQ">Antártida</option>
                                        <option value="AG">Antigua y Barbuda</option>
                                        <option value="AN">Antillas Holandesas</option>
                                        <option value="SA">Arabia Saudí</option>
                                        <option value="DZ">Argelia</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaiyán</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrein</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BE">Bélgica</option>
                                        <option value="BZ">Belice</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermudas</option>
                                        <option value="BY">Bielorrusia</option>
                                        <option value="MM">Birmania</option>
                                        <option value="BO">Bolivia</option>
                                        <option value="BA">Bosnia y Herzegovina</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BR">Brasil</option>
                                        <option value="BN">Brunei</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="BT">Bután</option>
                                        <option value="CV">Cabo Verde</option>
                                        <option value="KH">Camboya</option>
                                        <option value="CM">Camerún</option>
                                        <option value="CA">Canadá</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CY">Chipre</option>
                                        <option value="VA">Ciudad del Vaticano (Santa Sede)</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comores</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, República Democrática del</option>
                                        <option value="KR">Corea</option>
                                        <option value="KP">Corea del Norte</option>
                                        <option value="CI">Costa de Marfíl</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="HR">Croacia (Hrvatska)</option>
                                        <option value="CU">Cuba</option>
                                        <option value="DK">Dinamarca</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egipto</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="AE">Emiratos Árabes Unidos</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="SI">Eslovenia</option>
                                        <option value="ES">España</option>
                                        <option value="US">Estados Unidos</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Etiopía</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="PH">Filipinas</option>
                                        <option value="FI">Finlandia</option>
                                        <option value="FR">Francia</option>
                                        <option value="GA">Gabón</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GD">Granada</option>
                                        <option value="GR">Grecia</option>
                                        <option value="GL">Groenlandia</option>
                                        <option value="GP">Guadalupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GY">Guayana</option>
                                        <option value="GF">Guayana Francesa</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GQ">Guinea Ecuatorial</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="HT">Haití</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HU">Hungría</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IQ">Irak</option>
                                        <option value="IR">Irán</option>
                                        <option value="IE">Irlanda</option>
                                        <option value="BV">Isla Bouvet</option>
                                        <option value="CX">Isla de Christmas</option>
                                        <option value="IS">Islandia</option>
                                        <option value="KY">Islas Caimán</option>
                                        <option value="CK">Islas Cook</option>
                                        <option value="CC">Islas de Cocos o Keeling</option>
                                        <option value="FO">Islas Faroe</option>
                                        <option value="HM">Islas Heard y McDonald</option>
                                        <option value="FK">Islas Malvinas</option>
                                        <option value="MP">Islas Marianas del Norte</option>
                                        <option value="MH">Islas Marshall</option>
                                        <option value="UM">Islas menores de Estados Unidos</option>
                                        <option value="PW">Islas Palau</option>
                                        <option value="SB">Islas Salomón</option>
                                        <option value="SJ">Islas Svalbard y Jan Mayen</option>
                                        <option value="TK">Islas Tokelau</option>
                                        <option value="TC">Islas Turks y Caicos</option>
                                        <option value="VI">Islas Vírgenes (EEUU)</option>
                                        <option value="VG">Islas Vírgenes (Reino Unido)</option>
                                        <option value="WF">Islas Wallis y Futuna</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italia</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japón</option>
                                        <option value="JO">Jordania</option>
                                        <option value="KZ">Kazajistán</option>
                                        <option value="KE">Kenia</option>
                                        <option value="KG">Kirguizistán</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="LA">Laos</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LV">Letonia</option>
                                        <option value="LB">Líbano</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libia</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lituania</option>
                                        <option value="LU">Luxemburgo</option>
                                        <option value="MK">Macedonia, Ex-República Yugoslava de</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MY">Malasia</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MV">Maldivas</option>
                                        <option value="ML">Malí</option>
                                        <option value="MT">Malta</option>
                                        <option value="MA">Marruecos</option>
                                        <option value="MQ">Martinica</option>
                                        <option value="MU">Mauricio</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">México</option>
                                        <option value="FM">Micronesia</option>
                                        <option value="MD">Moldavia</option>
                                        <option value="MC">Mónaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Níger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk</option>
                                        <option value="NO">Noruega</option>
                                        <option value="NC">Nueva Caledonia</option>
                                        <option value="NZ">Nueva Zelanda</option>
                                        <option value="OM">Omán</option>
                                        <option value="NL">Países Bajos</option>
                                        <option value="PA">Panamá</option>
                                        <option value="PG">Papúa Nueva Guinea</option>
                                        <option value="PK">Paquistán</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Perú</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PF">Polinesia Francesa</option>
                                        <option value="PL">Polonia</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="UK">Reino Unido</option>
                                        <option value="CF">República Centroafricana</option>
                                        <option value="CZ">República Checa</option>
                                        <option value="ZA">República de Sudáfrica</option>
                                        <option value="DO">República Dominicana</option>
                                        <option value="SK">República Eslovaca</option>
                                        <option value="RE">Reunión</option>
                                        <option value="RW">Ruanda</option>
                                        <option value="RO">Rumania</option>
                                        <option value="RU">Rusia</option>
                                        <option value="EH">Sahara Occidental</option>
                                        <option value="KN">Saint Kitts y Nevis</option>
                                        <option value="WS">Samoa</option>
                                        <option value="AS">Samoa Americana</option>
                                        <option value="SM">San Marino</option>
                                        <option value="VC">San Vicente y Granadinas</option>
                                        <option value="SH">Santa Helena</option>
                                        <option value="LC">Santa Lucía</option>
                                        <option value="ST">Santo Tomé y Príncipe</option>
                                        <option value="SN">Senegal</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leona</option>
                                        <option value="SG">Singapur</option>
                                        <option value="SY">Siria</option>
                                        <option value="SO">Somalia</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="PM">St Pierre y Miquelon</option>
                                        <option value="SZ">Suazilandia</option>
                                        <option value="SD">Sudán</option>
                                        <option value="SE">Suecia</option>
                                        <option value="CH">Suiza</option>
                                        <option value="SR">Surinam</option>
                                        <option value="TH">Tailandia</option>
                                        <option value="TW">Taiwán</option>
                                        <option value="TZ">Tanzania</option>
                                        <option value="TJ">Tayikistán</option>
                                        <option value="TF">Territorios franceses del Sur</option>
                                        <option value="TP">Timor Oriental</option>
                                        <option value="TG">Togo</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad y Tobago</option>
                                        <option value="TN">Túnez</option>
                                        <option value="TM">Turkmenistán</option>
                                        <option value="TR">Turquía</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UA">Ucrania</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistán</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="VN">Vietnam</option>
                                        <option value="YE">Yemen</option>
                                        <option value="YU">Yugoslavia</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabue</option>
                                      </select>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="comercializador">Comercializador</label>
                                    <input class="form-control" id="comercializador" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fotografia_frontal">Fotografia de caja frontal</label>
                                    <input id="fotografia_frontal" type="file" class="form-control" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fotografia_trasera">Fotografia de caja trasera</label>
                                    <input id="fotografia_trasera" type="file" class="form-control" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fotografia_superior">Fotografia de caja superior</label>
                                    <input id="fotografia_superior" type="file" class="form-control" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fotografia_inferior">Fotografia de caja inferior</label>
                                    <input id="fotografia_inferior" type="file" class="form-control" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fotografia_izquierda">Fotografia de caja lateral izquierda</label>
                                    <input id="fotografia_izquierda" type="file" class="form-control" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="fotografia_derecha">Fotografia de caja lateral derecha</label>
                                    <input id="fotografia_derecha" type="file" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <a type="button" onclick="document.getElementById('siguiente_1').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                    <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                </svg>
                            </a>
                            <a type="button" onclick="document.getElementById('siguiente_3').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-right-square-fill" viewBox="0 0 16 16">
                                    <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z"/>
                                </svg>
                            </a>
                            <button id="siguiente_3" class="d-none btn btn-primary mx-4" data-bs-toggle="tab" data-bs-target="#nav-producto">Siguiente</button>
                        </div>
                    </div>

                    {{-- Expediente --}}

                    <div class="tab-pane fade" id="nav-expediente" role="tabpanel" aria-labelledby="nav-expediente-tab">
                        <div>
                            <h1 class="mb-4">Expediente clinico</h1>
                            <div class="form-group row">
                                <div class="mb-3 col-12">
                                    <label class="input-group-text" for="diagnostico_paciente">Diagnostico</label>
                                    <textarea class="form-control" id="diagnostico_paciente" rows="5" required></textarea>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="receta">Receta medica</label>
                                    <input class="form-control" id="receta" type="file" required>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="fecha_receta" class="input-group-text">Fecha de receta</label>
                                    <div class="">
                                        <div>
                                            <input type="date" class="form-control" id="fecha_receta" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="informe">Informe medico</label>
                                    <input class="form-control" id="informe" type="file" required>
                                </div>
                                <div class="col-12 mb-3 col-md-6">
                                    <label for="fecha_informe" class="input-group-text">Fecha de informe</label>
                                    <div class="">
                                        <div>
                                            <input type="date" class="form-control" id="fecha_informe" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="comprobante">Comprobante de domicilio</label>
                                    <input class="form-control" id="comprobante" type="file" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="identificacion">Identificación</label>
                                    <input class="form-control" id="identificacion" type="file" required>
                                </div>
                                <div class="col-12 col-md-6 mb-3">
                                    <label class="input-group-text" for="curp">CURP</label>
                                    <input class="form-control" id="curp" type="file" required>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <a type="button" onclick="document.getElementById('siguiente_2').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                    <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                </svg>
                            </a>
                            <a type="button" onclick="document.getElementById('siguiente_4').click()">
                                <svg style="background-color: white; color: red;" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-right-square-fill" viewBox="0 0 16 16">
                                    <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z"/>
                                </svg>
                            </a>
                            <button id="siguiente_4" class="d-none btn btn-primary mx-4" data-bs-toggle="tab" data-bs-target="#nav-expediente">Siguiente</button>
                        </div> 
                    </div>

                    {{-- Import Export --}}

                    <div class="tab-pane fade" id="nav-import-export" role="tabpanel" aria-labelledby="nav-producto-tab">
                        <div id="tramite_de_permiso">
                           <div class="form-group row pt-5">
                                <div class="col-6 mb-3">
                                    <div class="card">
                                        <label class="card-header" for="solicitud_llenada">Solicitud llenada</label>
                                        <div class="card-body justify-content-evenly d-flex">
                                            <div class="form-check-inline">
                                                <input class="" id="si" type="radio" name="solicitud_llenada" checked required>
                                                <label class="form-check-label" for="si">
                                                    Si
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <input class="" id="no" type="radio" name="solicitud_llenada" required>
                                                <label class="form-check-label" for="no">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="ingreso_a_tramite">Ingreso a tramite</label>
                                    <input class="form-control" id="ingreso_a_tramite" type="date" required>
                                </div>
                                <div class="col-6">
                                    <label class="input-group-text" for="recepcion_de_permiso">Permiso Cofepris</label>
                                    <input class="form-control" id="recepcion_de_permiso" type="file" required>
                                </div>
                           </div>            
                            <hr class="my-4">
                        </div>
                        <div id="global">
                            <div class="form-group row">
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="recibo_de_permiso">Recibo de permiso</label>
                                    <input class="form-control" id="recibo_de_permiso" type="file" required>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="envio_de_proforma_a_ac">Envio de proforma a AC</label>
                                    <input class="form-control" id="envio_de_proforma_a_ac" type="file" required>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="ac_confirma_proforma">AC confirma proforma</label>
                                    <input class="form-control" id="ac_confirma_proforma" required>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="ac_confirma_despacho">AC confirma despacho</label>
                                    <input class="form-control" id="ac_confirma_despacho" required>
                                </div>
                                <div class="col-6 mb-3">
                                    <label class="input-group-text" for="global_envia_awb_a_ac">Global envia AWB a AC</label>
                                    <input class="form-control" id="global_envia_awb_a_ac" type="file" required>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button id="submit_expediente" class="btn btn-primary"><a href="{{ route("home") }}" style="color: white; text-decoration: none;">Registrar paciente</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="d-flex justify-content-center my-5 py-5">
        <div class="input-flex-container" style="justify-content: center">
            <input class="mx-4 radio-line" type="radio" name="timeline-dot" data-description="1910">
            <div class="dot-info" data-description="1910">
                <span class="year">1</span>
                <span class="label"></span>
            </div>
            <input class="mx-4 radio-line" type="radio" name="timeline-dot" data-description="1920">
            <div class="dot-info" data-description="1920">
                <span class="year">2</span>
                <span class="label"></span>
            </div>
            <input class="mx-4 radio-line" type="radio" name="timeline-dot" data-description="1930" checked>
            <div class="dot-info" data-description="1930">
                <span class="year">3</span>
                <span class="label"></span>
            </div>
            <input class="mx-4 radio-line" type="radio" name="timeline-dot" data-description="1940">
            <div class="dot-info" data-description="1940">
                <span class="year">4</span>
                <span class="label"></span>
            </div>
            <input class="mx-4 radio-line" type="radio" name="timeline-dot" data-description="1950">
            <div class="dot-info" data-description="1950">
                <span class="year">5</span>
                <span class="label"></span>
            </div>
            <input class="mx-4 radio-line" type="radio" name="timeline-dot" data-description="1960">
            <div class="dot-info" data-description="1960">
                <span class="year">6</span>
                <span class="label"></span>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
            $(document).ready(function () {

                $("#siguiente_0").click(function (e) { 
                    $("#nav-paciente-tab").removeClass("disabled");
                    $("#nav-medico-tab").addClass("disabled");
                    document.getElementById("nav-paciente-tab").click();
                });

                $("#siguiente_1").click(function (e) { 
                    e.preventDefault();
                    if (
                    $("#nombres_paciente").val() &&
                    $("#appellido_paterno_paciente").val() &&
                    $("#appellido_materno_paciente").val() &&
                    $("#nacimiento").val() &&
                    ($("#edad").val()>0) &&
                    ($("#masculino").val() ||
                    $("#femenino").val()) &&
                    $("#calle_o_avenida").val() &&
                    $("#numero_interior").val() &&
                    $("#numero_exterior").val() &&
                    $("#colonia_o_fraccionamiento").val() &&
                    $("#codigo_postal").val() &&
                    $("#ciudad_paciente").val() &&
                    $("#estado_paciente").val() &&
                    $("#pais_paciente").val() &&
                    $("#telefono_paciente").val() &&
                    $("#email_paciente").val()
                    ) {
                        $("#nav-medico-tab").removeClass("disabled");
                        $("#nav-paciente-tab").addClass("disabled");
                        $("#alerta_paciente").addClass("d-none");
                        document.getElementById("nav-medico-tab").click();
                    }
                    else{
                        $("#alerta_paciente").removeClass("d-none");
                    }
                });

                $("#siguiente_2").click(function (e) { 
                    e.preventDefault();
                    if (
                    $("#nombres_medico").val() &&
                    $("#apellido_paterno_medico").val() &&
                    $("#apellido_materno_medico").val() &&
                    $("#ciudad_medico").val() &&
                    $("#estado_medico").val() &&
                    $("#telefono_medico").val() &&
                    $("#email_medico").val() &&
                    $("#cedula").val() &&
                    $("#especialidad").val() &&
                    $("#subespecialidad").val() &&
                    $("#cedula_especialidad").val()
                    ) {
                        $("#nav-producto-tab").removeClass("disabled");
                        $("#nav-medico-tab").addClass("disabled");
                        $("#alerta_paciente2").addClass("d-none");
                        document.getElementById("nav-producto-tab").click();
                    }
                    else{
                        $("#alerta_paciente2").removeClass("d-none");
                    }
                });

                $("#siguiente_3").click(function (e) { 
                    e.preventDefault();
                    if (
                    $("#marca").val() &&
                    $("#principio_activo").val() &&
                    $("#presentacion").val() &&
                    $("#lote").val() &&
                    $("#caducidad").val() &&
                    ($("#seco").val() ||
                    $("#refrigerado").val()) &&
                    $("#femenino").val() &&
                    $("#fabricante").val() &&
                    $("#pais_producto").val() &&
                    $("#comercializador").val() &&
                    $("#fotografia_frontal").val() &&
                    $("#fotografia_trasera").val() &&
                    $("#fotografia_superior").val() &&
                    $("#fotografia_inferior").val() &&
                    $("#fotografia_derecha").val() &&
                    $("#fotografia_izquierda").val()
                    ) {
                        $("#nav-expediente-tab").removeClass("disabled");
                        $("#nav-producto-tab").addClass("disabled");
                        $("#alerta_paciente").addClass("d-none");
                        document.getElementById("nav-expediente-tab").click();
                    }
                    else{
                        $("#alerta_paciente").removeClass("d-none");
                    }
                });

                $("#siguiente_4").click(function (e) { 
                    e.preventDefault();
                    if (
                    $("#diagnostico_paciente").val() &&
                    $("#receta").val() &&
                    $("#fecha_receta").val() &&
                    $("#informe").val() &&
                    $("#fecha_informe").val() &&
                    $("#comprobante").val() &&
                    $("#identificacion").val() &&
                    $("#curp").val()
                    ) {
                        $("#nav-import-export-tab").removeClass("disabled");
                        $("#nav-expediente-tab").addClass("disabled");
                        $("#alerta_paciente3").addClass("d-none");
                        document.getElementById("nav-import-export-tab").click();
                    }
                    else{
                        $("#alerta_paciente3").removeClass("d-none");
                    }
                });
            });
    </script>
</div>
@endsection
