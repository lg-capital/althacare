<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Chart</title>
    </head>
    <body>
        {{$chart->container() }}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset=utf-8></script>
        {{ $chart->script() }}
    </body>
</html>