@csrf
<div class="mb-3">
    <label class="form-label" for="">Titulo del proyecto</label><br>
    <input class="form-control bg-ligth shadow-sm" name="title" type="text" value="{{old('title',$proyecto->title)}}">
</div>
<div class="mb-3">
    <label class="form-label" for="">URL</label><br>
    <input class="form-control bg-ligth shadow-sm" name="url" type="text" value="{{ old('url',$proyecto->url) }}">
</div>
<div class="mb-3">
    <label class="form-label" for="">Descripción del proyecto</label><br>
    <textarea class="form-control bg-ligth shadow-sm" name="description" id="" cols="30" rows="10">{{ old('description',$proyecto->description)}}</textarea>
</div>
<div class="d-grid gap-2">
    <button class="btn btn-primary text-white" type="submit">{{ $btnText }}</button>
</div>