@extends('layout')
@section('titulo','Tablero de control')
@section('content')
<div class=" mb-5 pb-2 pt-5 pb-5">
    <div style="margin-bottom: 360px">
        <div class="mb-4 d-flex justify-content-between align-items-center">
            <h1 class="mb-0 display-4 mb-4">Tablero de control</h1>
            @auth
                <a class="btn btn-primary text-white" href="{{ route('project.create') }}">Paciente nuevo</a>        
            @endauth
        </div>
        <hr>
        <p class="lead text-secondary">Pacientes en tratamiento</p>
        <table class="table mb-5 pb-5">
            <thead>
                <tr>
                    <th scope="col">Folio</th>
                    <th scope="col">Paciente</th>
                    <th scope="col">Producto</th>
                    <th scope="col">Lote</th>
                    <th scope="col">Caducidad</th>
                    <th scope="col">Medico tratante</th>
                    <th scope="col">Diagnostico</th>
                    <th scope="col">Fecha de creación</th>
                    <th scope="col">Fecha de actualización</th>
                    <th scope="col">Mostrar</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($projects as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->url }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->created_at->format('d/m/Y') }}</td>
                    <td>{{ $item->updated_at->format('d/m/Y') }}</td>
                    <td><a class="d-flex justify-content-between align-items-center" style="text-decoration: none" href="{{ route('project.show',  $item->url ) }}">Mostrar</a></td>
                </tr>
                @empty
                    <h1>No hay registros por mostrar</h1>     
                                
                @endforelse
                
            </tbody>
        </table>
        
        {{-- <ul class="list-group">
            @forelse ($projects as $item)
                <li class="list-group-item border-0 mb-3 shadow">
                    <a class="d-flex justify-content-between align-items-center" style="text-decoration: none" href="{{ route('project.show',  $item->url ) }}">
                        <span class="text-secondary font-weight-bold">{{ $item->title }}</span>
                        <span class="text-black-50">{{ $item->created_at->format('d/m/Y') }}</span>
                    </a>
                </li>        
            @empty
                <li class="list-group-item border-0 mb-3 shadow">
                    No hay portafolios para mostrar
                </li>        
                            
            @endforelse
        </ul> --}}
        {{ $projects->links() }}
    </div>
</div>



@endsection
