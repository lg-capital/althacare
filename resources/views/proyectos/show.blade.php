@extends('layout')
@section('titulo','Protafolio | ' . $proyecto->title)
@section('content')
<div class="container">
    <div class="bg-white p-5 shadow rounded">
        <h1>{{ $proyecto->title }}</h1>     
        <p class="text-secondary">{{ $proyecto->description }}</p>
        <p class="text-black-50">{{ $proyecto->updated_at->diffForHumans() }}</p>
        @auth
        <div class="d-flex justify-content-between">
            <a style="text-decoration: none" href="{{ route('project.index') }}">Regresar</a>
            <div class="btn-group btn-group-sm">
                <a class="btn text-white btn-primary" href="{{ route('project.edit', $proyecto) }}">Editar</a>
                <a class="btn text-white btn-danger" href="#" onclick="document.getElementById('delete_element').submit()">Eliminar</a>
            </div>
        </div>
        <form class="d-none" id="delete_element" action="{{ route('project.destroy', $proyecto) }}" method="POST">
            @csrf
            @method('DELETE')
        </form>
        @endauth
    </div>
</div>
@endsection