@extends('layout')
@section('titulo','Editar | ' . $proyecto->title)
@section('content')
<div class="container mb-4">
    <div class="row">
        <div class="col-12 col-sm-10 col-md-10 col-xl-6 mx-auto">
            @include('partials.validation-errors')
            <form class="bg-white shadow rounded py-3 px-3" action="{{ route('project.update',$proyecto) }}" method="POST">
                <h1 class="display-4">{{ $proyecto->title }}</h1>
                <hr>
                @method('PATCH')
                @include('proyectos._form',['btnText'=>'Actualizar'])
            </form>
        </div>
    </div>
</div>
@endsection