@extends('layout')
@section('titulo','Portafolio')
@section('content')
<div class="container mb-4">
    <div class="row">
        <div class="col-12 col-sm-10 col-md-10 col-xl-6 mx-auto">
            @include('partials.validation-errors')
            <form class="bg-white shadow rounded py-3 px-3" action="{{ route('project.store') }}" method="POST">
                <h1 class="display-4">Crear un nuevo proyecto</h1>
                <hr>
                @include('proyectos._form',['btnText'=>'Guardar'])
            </form>
        </div>
    </div>
</div>
@endsection