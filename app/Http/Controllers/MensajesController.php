<?php

namespace App\Http\Controllers;

use App\Mail\MensajeRecibido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MensajesController extends Controller
{
    //
    public function store(){
        $mensaje = request()->validate([
            'nombre'=> 'required',
            'asunto'=> 'required',
            'mensaje'=> 'required',
            'email'=>'required|email'
        ],[
            'nombre.required'=>'Necesitamos tu nombre'
        ]);
        Mail::to('christos.marroquin@hotmail.com')->send(new MensajeRecibido($mensaje));
        return back()->with('status','Recibimos tu mensaje te responderemos pronto');
    }
}
