<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveProjectRequest;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Charts\prueba;
use App\Charts\prueba2;


class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')
        ->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $portafolio=DB::table('projects')->get();

        $projects=Project::paginate(12);

        $chart = new prueba();
        $chart->labels(['One', 'Two', 'Three','Four' ,'Five']);
        $chart->dataset('My dataset 1', 'line', [1, 2, 3, 4,3]);
        $chart->dataset('My dataset 2', 'line', collect([3, 4, 5, 6,3]));

        $chart2 = new prueba2();
        $chart2->labels(['One', 'Two', 'Three','Four' ,'Five']);
        $chart2->dataset('My dataset 1', 'bar', [1, 2, 3, 4,3]);
        $chart2->dataset('My dataset 2', 'bar', collect([3, 4, 5, 6,3]));

        $chart3 = new prueba();
        $chart3->labels(['One', 'Two', 'Three','Four' ,'Five']);
        $chart3->dataset('My dataset 1', 'doughnut', [1, 2, 3, 4,3]);
        $chart3->dataset('My dataset 2', 'doughnut', collect([3, 4, 5, 6,3]));

        $chart4 = new prueba();
        $chart4->labels(['One', 'Two', 'Three','Four' ,'Five']);
        $chart4->dataset('My dataset 1', 'pie', [1, 2, 3, 4,3]);
        $chart4->dataset('My dataset 2', 'pie', collect([3, 4, 5, 6,3]));

        return view('proyectos.index',compact('projects','chart','chart2','chart3','chart4'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('expediente', ['proyecto'=> new Project]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveProjectRequest $request)
    {
        //        
        Project::create($request->validated());
        return redirect()->route('project.index')->with(['status'=>'El proyecto fue guardado con exito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $proyecto)
    {
        //
        
        return view('proyectos.show',compact('proyecto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $proyecto)
    {
        //
        return view('proyectos.edit',compact('proyecto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $proyecto, SaveProjectRequest $request)
    {
        //
        $proyecto->update($request->validated());
        return redirect()->route('project.show',$proyecto->url)->with(['status'=>'El proyecto fue actualizado con exito']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $proyecto)
    {
        //
        $proyecto->delete();
        return redirect()->route('project.index')->with(['status'=>'El proyecto fue eliminado con exito']);
    }
}
