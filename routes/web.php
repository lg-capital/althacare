<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Charts\prueba;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $nombre="Juan";
//     return view('home',compact('nombre'));
// })->name('home');

Route::get('/grafica', function(){
    $chart = new prueba();
    $chart->labels(['One', 'Two', 'Three','Four' ,'Five']);
    $chart->dataset('My dataset 1', 'line', [1, 2, 3, 4,0]);
    $chart->dataset('My dataset 2', 'line', collect([3, 4, 5, 6,0]));
    return view('grafica', compact('chart'));
})->name('grafica');

$controller_path = 'App\Http\Controllers\\';

Route::view('/','home')
->name('home')->middleware('auth');

Route::view('/about','about')
->name('about')->middleware('auth');

Route::view('/contacto','contacto')
->name('contacto')->middleware('auth');

Route::resource('tablero-de-control',$controller_path.'ProjectController')
->names('project')
->parameters(['tablero-de-control'=>'proyecto'])
->middleware('auth');

Route::post('contacto',$controller_path . 'MensajesController@store')->middleware('auth');
// Route::resource('portafolio',$controller_path . 'PortafolioController');
 
Auth::routes(['register'=> true]);

// Contenido de expedientes.

// Route::view('/usocompasivo-roche','expediente')
// ->name('expediente')->middleware('auth');

Route::get('/reportes',function(){
    $chart = new prueba();
    $chart->labels(['One', 'Two', 'Three','Four' ,'Five']);
    $chart->dataset('My dataset 1', 'line', [1, 2, 3, 4,3]);
    $chart->dataset('My dataset 2', 'line', collect([3, 4, 5, 6,3]));

    $chart2 = new prueba();
    $chart2->labels(['One', 'Two', 'Three','Four' ,'Five']);
    $chart2->dataset('My dataset 1', 'bar', [1, 2, 3, 4,3]);
    $chart2->dataset('My dataset 2', 'bar', collect([3, 4, 5, 6,3]));

    $chart3 = new prueba();
    $chart3->labels(['One', 'Two', 'Three','Four' ,'Five']);
    $chart3->dataset('My dataset 1', 'doughnut', [1, 2, 3, 4,3]);
    $chart3->dataset('My dataset 2', 'doughnut', collect([3, 4, 5, 6,3]));

    $chart4 = new prueba();
    $chart4->labels(['One', 'Two', 'Three','Four' ,'Five']);
    $chart4->dataset('My dataset 1', 'pie', [1, 2, 3, 4,3]);
    $chart4->dataset('My dataset 2', 'pie', collect([3, 4, 5, 6,3]));

    return view('reportes', compact('chart','chart2','chart3','chart4'));
})
->name('reportes')->middleware('auth');

